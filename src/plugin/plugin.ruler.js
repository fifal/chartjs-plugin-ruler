'use strict'


const pluginName = 'ruler'

// TODO: default values for rulers and calculate button
// TODO: x values are not moment objects - drawDateTime 

/**
 * Checks if configuration contains all required fields
 * @param {Chart} chartInstance 
 */
function checkPluginConfiguration(chartInstance) {
    const requiredFields = {
        'onCalculate': function () { console.warn('not implemented yet...') },
        'labelFont': 'bold 8pt Sans-Serif',
    }

    if (!(pluginName in chartInstance.options.plugins)) {
        console.warn("Options are missing configuration for the 'ruler' plugin.")
        chartInstance.options.plugins[pluginName] = {}
    }

    Object.keys(requiredFields).forEach((field) => {
        if (!(field in chartInstance.options.plugins[pluginName])) {
            console.warn("Plugin 'ruler' configuration is missing '" + field + "', using default value.")
            chartInstance.options.plugins[pluginName][field] = requiredFields[field]
        }
    })

    chartInstance[pluginName] = {
        toolbarDrawn: false,
        rulers: [],
        imageData: null,
        rulerButtons: [],
        calculateButton: null,
        chartInstance: null,
        shouldRecalculate: false,
    }
}

/**
 * Checks if an event occurred in the chart boundaries
 *  
 * @param {MouseEvent} event 
 * @param {Chart} chartArea 
 */
function isEventInBoundaries(event, chart) {
    const boundingRect = chart.canvas.getBoundingClientRect()

    const x = event.clientX - boundingRect.left
    const y = event.clientY - boundingRect.top

    // NOTE: the Y axis is switched
    if (x < chart.chartArea.left || x > chart.chartArea.right ||
        y < chart.chartArea.top || y > chart.chartArea.bottom) {
        return false
    }

    return true
}

/**
 * Handler for the mouse move event in the chart area
 * @param {MouseEvent} event 
 * @param {Chart} chartInstance 
 */
function onMouseMove(event, chartInstance) {
    Object.values(chartInstance[pluginName].rulers).forEach(ruler => {
        if (ruler.active && isEventInBoundaries(event, chartInstance)) {
            const ctx = chartInstance.canvas.getContext('2d')
            const rect = chartInstance.canvas.getBoundingClientRect()

            if (chartInstance[pluginName].imageData === null) {
                chartInstance[pluginName].imageData = ctx.getImageData(0, 0, chartInstance.canvas.width, chartInstance.canvas.height)
            }

            const xPos = event.clientX - rect.left
            const xScale = Object.values(chartInstance.scales).find(scale => (scale.position === 'bottom' || scale.position === 'top'))
            const value = xScale.getValueForPixel(xPos)

            ctx.putImageData(chartInstance[pluginName].imageData, 0, 0)
            ruler.x = xPos
            ruler.event = event
            ruler.value = value

            drawRuler(ruler, chartInstance)
        }
    })
}

/**
 * Handler for the mouse click in the chart area
 * @param {MouseEvent} event 
 */
function onMouseClick(event) {
    const activeRuler = this[pluginName].rulers.find((ruler) => ruler.active)
    const rect = this.canvas.getBoundingClientRect()

    if (activeRuler === undefined) {
        return
    }
    this[pluginName].imageData = null

    activeRuler.x = event.clientX - rect.left
    activeRuler.active = false

    toggleCalculateButton(this)
    toggleRulerButtons(this)
    this.options.plugins[pluginName]?.onRulersChanged(this[pluginName].rulers)
}

/** 
 * Toggles the calculate button - if there are more than two rulers in chart -> enable, otherwise disable
 */
function toggleCalculateButton(chartInstance) {
    const rulersWithValue = chartInstance[pluginName].rulers.filter(ruler => ruler.x !== null).length
    chartInstance[pluginName].calculateButton.disabled = rulersWithValue < 2
}

/**
 * Toggles the ruler buttons, only one can be active at time when adding new 
 * @param {Chart} chartInstance 
 */
function toggleRulerButtons(chartInstance) {
    const activeRuler = chartInstance[pluginName].rulers.find(ruler => ruler.active)

    if (activeRuler) {
        Object.values(chartInstance[pluginName].rulerButtons).forEach(element => {
            if (element.key !== activeRuler.id) {
                element.value.disabled = true
            }
        })
    }
    else {
        Object.values(chartInstance[pluginName].rulerButtons).forEach(element => { element.value.disabled = false })
    }

    chartInstance[pluginName].rulers.forEach(ruler => {
        const button = Object.values(chartInstance[pluginName].rulerButtons).find(element => element.key === ruler.id)

        if (ruler.x !== null || ruler.active) {
            button.value.innerHTML = chartInstance.options.plugins[pluginName].rulers[ruler.id].innerHtmlRemove
        } else {
            button.value.innerHTML = chartInstance.options.plugins[pluginName].rulers[ruler.id].innerHtml
        }
    })
}

/**
 * Draws ruler into the chart 
 * @param {object} ruler 
 * @param {Chart} chartInstance 
 */
function drawRuler(ruler, chartInstance) {
    const ctx = chartInstance.canvas.getContext('2d')

    ctx.save()
    ctx.fillStyle = ruler.color
    ctx.fillRect(ruler.x, chartInstance.chartArea.bottom, 2, chartInstance.chartArea.top - chartInstance.chartArea.bottom)
    ctx.restore()

    drawDateTime(ruler, chartInstance)
    drawSignalValues(ruler, chartInstance)
}

/**
 * Draws a datetime on the top of the ruler
 * @param {object} ruler 
 * @param {Chart} chartInstance 
 */
function drawDateTime(ruler, chartInstance) {
    const ctx = chartInstance.canvas.getContext('2d')

    ctx.save()
    ctx.font = chartInstance.options.plugins[pluginName]['labelFont']
    const text = ruler.value.format('DD/MM/YY HH:mm:ss')
    const height = getFontHeight(ctx.font)

    // Check if ruler text fits into the chart area
    const textWidth = ctx.measureText(text).width + 1

    let positionX = ruler.x + 4
    if (ruler.x + textWidth > chartInstance.chartArea.right) {
        positionX = ruler.x - textWidth - 4
    }

    ctx.fillStyle = 'white'
    ctx.fillRect(positionX, chartInstance.chartArea.top + 1, textWidth + 1, height)
    ctx.fillStyle = ruler.color
    ctx.fillText(text, positionX + 1, chartInstance.chartArea.top + height - 3)
    ctx.restore()
}

/**
 * Finds the closest point using binary search 
 * @param {moment} timestamp 
 * @param {Array} values 
 * @returns 
 */
function binarySearch(timestamp, values) {
    if (values.length === 0){
        return null
    }

    let min = 0
    let max = values.length

    while (min <= max) {
        let half = Math.round((min + max) / 2)

        if (timestamp.diff(values[half]['x']) < 0) {
            max = half
        } else if (timestamp.diff(values[half]['x']) > 0) {
            min = half
        } else {
            return half
        }

        if ((max - min) <= 1) {
            return min
        }
    }
}

/**
 * Draws signal values next to the ruler 
 * @param {object} ruler 
 * @param {Chart} chartInstance 
 */
function drawSignalValues(ruler, chartInstance) {
    const ctx = chartInstance.canvas.getContext('2d')
    const points = chartInstance.getElementsAtEventForMode(ruler.event, 'x', { intersect: false })

    const signals = []
    chartInstance.data.datasets.forEach((dataset, i) => {
        if(dataset.hidden){
            return
        }

        const axisId = dataset.yAxisID
        let index = points.find(p => p._datasetIndex === i)?._index

        // We didn't find index by default chart methods (getElementAtEvent), lets try to find it ourselves
        if (!index) {
            index = binarySearch(ruler.value, dataset.data)
        }

        // Skip if not found
        if (index === null || index === undefined || dataset.data[index].y === null) {
            return
        }

        // We have a point and so we can draw the values
        if (index !== null && index !== undefined) {
            const sequence = `${i + 1}: `
            const scale = Object.values(chartInstance.scales).find(scale => scale.id === axisId)
            const value = dataset.data[index].y
            const pixel = scale.getPixelForValue(value)
            const text = sequence + value.toFixed(2)

            // Skip values that are out of bounds for given scale
            if (value > scale.max || value < scale.min) {
                return
            }

            signals.push({
                pixel: pixel,
                text: text,
                color: dataset.signal.color
            })
        }
    })

    let sortedSignals = Object.values(signals).sort((a, b) => {
        return a.pixel < b.pixel ? -1 : a.pixel > b.pixel ? 1 : 0
    })

    const fontStyle = chartInstance.options.plugins[pluginName]['labelFont']
    sortedSignals = fitSignalValues(sortedSignals, chartInstance.chartArea, fontStyle)
    const height = getFontHeight(fontStyle)

    sortedSignals.forEach(signal => {
        ctx.save()
        ctx.font = fontStyle

        // Check if ruler text fits into the chart area
        let textWidth = ctx.measureText(signal.text).width
        let positionX = ruler.x + 4
        if (ruler.x + textWidth > chartInstance.chartArea.right) {
            positionX = ruler.x - textWidth - 4
        }

        ctx.fillStyle = 'white'
        ctx.fillRect(positionX, signal.pixel - 12, textWidth + 1, height)
        ctx.fillStyle = signal.color
        ctx.fillText(signal.text, positionX + 1, signal.pixel - 2)
        ctx.restore()
    })
}

/**
 * Fits signal values next to the ruler so they don't overlap
 * @param {array} sortedSignals 
 * @param {Chart} chartArea 
 * @param {string} fontStyle
 */
function fitSignalValues(sortedSignals, chartArea, fontStyle) {
    const height = getFontHeight(fontStyle)
    let remainder = 0

    let evenDist = false
    for (const [index, signal] of sortedSignals.entries()) {
        // Move upper most signal down by height so it doesn't colide with time value
        if (index === 0) {
            signal.pixel += (2 * height)
            continue
        }

        if (index === sortedSignals.length - 1) {
            continue
        }

        signal.pixel += remainder
        const diff = signal.pixel - sortedSignals[index - 1].pixel
        if (diff < height) {
            remainder = (height - diff)
            signal.pixel += remainder
        }

        if (signal.pixel > chartArea.bottom || signal.pixel < chartArea.top) {
            evenDist = true
            break
        }
    }

    // If signals overflew evenly distribute them over the axis
    if (evenDist) {
        const step = (chartArea.bottom - chartArea.top + (2 * height)) / (sortedSignals.length + 1)
        const start = chartArea.top + (2 * height)

        for (const [index, signal] of sortedSignals.entries()) {
            signal.pixel = index * step + start
        }
    }

    return sortedSignals
}

/**
 * Creates a new HTML button 
 * @param {string} value 
 * @param {string} classNames 
 * @param {string} innerHtml 
 * @param {function} onClick 
 */
function createButton(value, classNames, innerHtml, onClick) {
    const button = document.createElement('button')
    button.onclick = onClick

    button.innerHTML = innerHtml
    button.value = value
    button.className = classNames
    return button
}

/**
 * Returns x axis values for each ruler 
 * @param {Chart} chartInstance 
 * @return {array}
 */
function getRulerTimestamps(chartInstance) {
    const result = {}
    Object.values(chartInstance[pluginName].rulers).forEach(ruler => {
        if (ruler.x === null) {
            return
        }

        // Get the first x axis
        const xScale = Object.values(chartInstance.scales).find(scale => (scale.position === 'bottom' || scale.position === 'top'))
        result[ruler.id] = xScale.getValueForPixel(ruler.x)
        result[ruler.id].color = ruler.color
    })

    return result
}

/**
 * Returns true if any ruler is active at the moment
 * @return {boolean}
 */
function isRulerActive(chartInstance) {
    let result = false
    Object.values(chartInstance[pluginName].rulers).forEach(ruler => {
        if (ruler.active) {
            result = true
        }
    })
    return result
}

/**
 * Returns font height
 * @param {String} fontStyle 
 */
function getFontHeight(fontStyle){
    return parseInt(fontStyle.match(/\d+/)) + 3 ?? 13
}

const RulerPlugin = {
    id: pluginName,

    beforeInit: (chartInstance) => {
        checkPluginConfiguration(chartInstance)

        // Listeners have to be added here so we can prevent other addon actions when ruler is active
        chartInstance.canvas.addEventListener('mousemove', function (e) {
            if (isRulerActive(chartInstance)) {
                e.stopImmediatePropagation()
                onMouseMove(e, chartInstance)
            }
        })
        chartInstance.canvas.addEventListener('click', onMouseClick.bind(chartInstance))
        chartInstance.canvas.addEventListener('mousedown', function (e) {
            if (isRulerActive(chartInstance)) {
                e.stopImmediatePropagation()
            }
        })
        chartInstance.canvas.addEventListener('mouseup', function (e) {
            if (isRulerActive(chartInstance)) {
                e.stopImmediatePropagation()
            }
        })

        RulerPlugin.chartInstance = chartInstance
    },

    /**
     * Add padding so we can fit our buttons
     * @param {Chart} chartInstance 
     */
    beforeLayout: (chartInstance) => {
        chartInstance.options.layout.padding.top += 35
    },

    /**
     * If ruler is active prevent hover over the point in chart 
     * @param {Chart} chartInstance
     * @param {MouseEvent} event
     */
    beforeEvent: (chartInstance, event) => {
        let result = true
        Object.values(chartInstance[pluginName].rulers).forEach(value => {
            if (value.active) {
                result = false
            }
        })

        return result
    },

    afterDraw: (chartInstance) => {
        const canvasOffsetTop = chartInstance.canvas.offsetTop

        if (!chartInstance[pluginName].toolbarDrawn) {
            const toolbar = document.createElement('span')
            toolbar.style.position = 'absolute'
            toolbar.style.zIndex = '3'
            toolbar.style.top = `${canvasOffsetTop}px`
            toolbar.style.right = '0px'

            const onClickFun = function (e) {
                const rul = chartInstance[pluginName].rulers.find(ruler => ruler.id === e.currentTarget.value)
                const color = chartInstance.options.plugins[pluginName].rulers[e.currentTarget.value].color
                if (!rul) {
                    chartInstance[pluginName].rulers.push({ active: true, id: e.currentTarget.value, color: color, value: null })
                } else {
                    if (rul.x !== null) {
                        rul.x = null
                        rul.active = false
                    } else {
                        rul.active = !rul.active
                    }
                }

                toggleCalculateButton(chartInstance)
                toggleRulerButtons(chartInstance)
                chartInstance.update()
                chartInstance.options.plugins[pluginName]?.onRulersChanged(chartInstance[pluginName].rulers)
            }

            for (const [key, value] of Object.entries(chartInstance.options.plugins[pluginName].rulers)) {
                const button = createButton(key, value.buttonClass, value.innerHtml, onClickFun)
                toolbar.appendChild(button)
                chartInstance[pluginName].rulerButtons.push({ key: key, value: button })
            }

            const calculateOptions = chartInstance.options.plugins[pluginName].calculateButton
            const calculateButton = createButton("calculate", calculateOptions.buttonClass, calculateOptions.innerHtml, () => {
                chartInstance.options.plugins[pluginName].onCalculate(getRulerTimestamps(chartInstance))
            })
            calculateButton.disabled = 'disabled'
            toolbar.appendChild(calculateButton)
            chartInstance[pluginName].calculateButton = calculateButton

            chartInstance.canvas.parentNode.appendChild(toolbar)
            chartInstance[pluginName].toolbarDrawn = true
        }

        Object.values(chartInstance[pluginName].rulers).forEach(ruler => {
            if (ruler.x === undefined || ruler.x === null) {
                return
            }

            // Recalculate ruler positions if the chart size changed
            if (ruler.value && chartInstance[pluginName].shouldRecalculate) {
                console.log('recalculating')
                const xScale = Object.values(chartInstance.scales).find(scale => (scale.position === 'bottom' || scale.position === 'top'))
                const pixel = xScale.getPixelForValue(ruler.value)
                ruler.x = pixel
            }

            drawRuler(ruler, chartInstance)
        })

        chartInstance[pluginName].shouldRecalculate = false
    },

    resize: (chartInstance) => {
        chartInstance[pluginName].shouldRecalculate = true
    },

    /**
     * Function resets all created rulers
     */
    resetRulers: (chartInstance) => {
        chartInstance[pluginName].rulers = []
        Object.values(chartInstance[pluginName].rulerButtons).forEach(element => {
            element.value.disabled = false
            element.value.innerHTML = chartInstance.options.plugins[pluginName].rulers[element.key].innerHtml
        })
        chartInstance[pluginName].calculateButton.disabled = true
        chartInstance[pluginName].imageData = null
    }
}

export default RulerPlugin 