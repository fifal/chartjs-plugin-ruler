## chartjs-plugin-ruler
[![npm version](https://badge.fury.io/js/chartjs-plugin-ruler.svg)](https://badge.fury.io/js/chartjs-plugin-ruler)

This plugins adds the possiblity to add rulers into the chart. When the calculate button is clicked a callback is called with the x-axis value for each ruler.
> NOTE: This plugin is highly specific for one application and I do NOT recommend using it.

![example image](doc/example.png)

### Installation
You can install the plugin using the npm:
```
npm install chartjs-plugin-ruler
```

### Usage
Example code on how to use this plugin:
```javascript
import Chart from 'chart.js'
import RulerPlugin from 'chartjs-plugin-ruler'

// Register plugin
Chart.plugins.register(RulerPlugin)

// Plugin options
const options = {
    plugins: {
        ruler: {
            rulers: {
                '1': {
                    buttonClass: 'btn btn-danger',
                    innerHtml: 'ruler1',
                    innerHtmlRemove: 'remove1',
                    color: 'red'
                },
                '2': {
                    buttonClass: 'btn btn-primary',
                    innerHtml: 'ruler2',
                    innerHtmlRemove: 'remove2',
                    color: 'blue'
                },
                '3': {
                    buttonClass: 'btn btn-success',
                    innerHtml: 'ruler3',
                    innerHtmlRemove: 'remove3',
                    color: 'green'
                }
            },
            calculateButton: {
                buttonClass: 'btn btn-info',
                innerHtml: 'calculate'
            },
            onCalculate: function(data){
                console.log(data)
            },
            onRulersChange: function(data){
                console.log(data)
            }
        }
    }
}

// Create Chart as usual
... 
```